//
//  PowerPenguin.h
//  Power Penguin
//
//  Created by Mathias Jensen on 1/21/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFHTTPRequestOperationManager.h>

@interface PowerPenguin : NSObject

+ (PowerPenguin *)instance;

- (BOOL)debug;

typedef void (^requestSuccess)(NSDictionary *response);
typedef void (^requestFailure)(NSDictionary *response);

#pragma mark - Device Requests
- (void)updateStatusOfDeviceWithID:(NSString *)name withSuccess:(requestSuccess)success andFailure:(requestFailure)failure;

#pragma mark - Device Management
- (BOOL)isConnected;
- (void)updateDevices;
- (void)updateDevicesWithSuccess:(requestSuccess)success andFailure:(requestFailure)failure;
- (NSDictionary *)devices;
- (NSArray *)deviceList;
- (NSDictionary *)deviceDict;

- (void)toggleDeviceWithID:(NSString *)name withSuccess:(requestSuccess)success andFailure:(requestFailure)failure;
- (void)toggleDevice:(NSString *)name toggle:(NSString *)t withSuccess:(requestSuccess)success andFailure:(requestFailure)failure;


- (int)activatedDevices;

@end
