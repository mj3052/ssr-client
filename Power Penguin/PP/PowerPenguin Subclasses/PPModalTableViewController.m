//
//  PPModalTableViewController.m
//  Power Penguin
//
//  Created by Mathias Jensen on 1/23/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import "PPModalTableViewController.h"

@interface PPModalTableViewController ()

@end

@implementation PPModalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.106 green:0.109 blue:0.120 alpha:1.000]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(popFromSender:)];
    
    self.navigationItem.rightBarButtonItem = button;
}

- (void)popFromSender:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
