//
//  PowerPenguin.m
//  Power Penguin
//
//  Created by Mathias Jensen on 1/21/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import "PowerPenguin.h"

@implementation PowerPenguin {
    bool debug;
    AFHTTPRequestOperationManager *m;
    NSString *base;
    NSMutableDictionary *devices;
}

static PowerPenguin *instance = nil;

+ (PowerPenguin *)instance
{
    if(instance == nil) {
        instance = [[super allocWithZone:NULL] init];
        
        // Init here
        instance->debug = YES;
        instance->m = [AFHTTPRequestOperationManager manager];
        instance->base = @"http://ssr.pxy.dk:4567";
        instance->devices = nil;
    }
    
    
    return instance;
}

+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized(self)
    {
        if (instance == nil)
        {
            instance = [super allocWithZone:zone];
            return instance;
        }
    }
    
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (BOOL)debug
{
    return instance->debug;
}

#pragma mark - Networking

- (NSString *)path:(NSString *)path
{
    return [instance->base stringByAppendingString:path];
}

- (void)getPath:(NSString *)path withSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
    [instance->m GET:[self path:path] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *response = nil;
        
        if (operation.responseString) {
            response = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
        }
        
        if (instance->debug) {
            NSLog(@"Success. Path: %@, Response: %@", path, response);
        }
        
        if (success) {
            success(response);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *response = nil;
        
        if (operation.responseString) {
            response = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
        }
        
        if (instance->debug) {
            NSLog(@"Error. Path: %@, Response: %@", path, response);
        }
        
        if (failure) {
            failure(response);
        }
    }];
}

#pragma mark - Device Requests
- (void)getDevicesWithSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
    [self getPath:@"/list" withSuccess:^(NSDictionary *response) {
        if (success) {
            success(response);
        }
    } andFailure:^(NSDictionary *response) {
        if (failure) {
            failure(response);
        }
    }];
}

- (void)updateStatusOfDeviceWithID:(NSString *)name withSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
    [self getPath:[NSString stringWithFormat:@"/device/%@/status", name] withSuccess:^(NSDictionary *response) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateDevicesWithSuccess:success andFailure:failure];
        });
    } andFailure:^(NSDictionary *response) {
        if (failure) {
            failure(response);
        }
    }];
}

- (void)toggleDeviceWithID:(NSString *)name toggle:(NSString *)toggle withSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
    [self getPath:[NSString stringWithFormat:@"/device/%@/%@", name, toggle] withSuccess:^(NSDictionary *response) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateDevicesWithSuccess:success andFailure:failure];
        });
    } andFailure:^(NSDictionary *response) {
        if (failure) {
            failure(response);
        }
    }];
}

#pragma mark - Device Management

- (BOOL)isConnected
{
    if (instance->devices != nil) {
        return YES;
    } else {
        return NO;
    }
}

- (void)updateDevices
{
    [self updateDevicesWithSuccess:nil andFailure:nil];
}

- (void)updateDevicesWithSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
    [self getDevicesWithSuccess:^(NSDictionary *response) {
        instance->devices = [NSMutableDictionary dictionaryWithDictionary:[response valueForKey:@"devices"]];
        if (success) {
            success(response);
        }
    } andFailure:^(NSDictionary *response) {
        if (failure) {
            failure(response);
        }
    }];
}

- (NSDictionary *)devices
{
    return instance->devices;
}

- (NSArray *)deviceList
{
    return [instance->devices valueForKey:@"list"];
}

- (NSDictionary *)deviceDict
{
    return [instance->devices valueForKey:@"dict"];
}

- (void)toggleDeviceWithID:(NSString *)name withSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
    NSDictionary *device = [[self deviceDict] valueForKey:name];
    
    NSLog(@"Status: %@", [device valueForKey:@"status"]);
    
    if ([[device valueForKey:@"status"] isEqualToString:@"0"]) {
        // Turn on
        if (instance->debug) {
            NSLog(@"Turning device on");
        }
        
        [self toggleDeviceWithID:name toggle:@"on" withSuccess:success andFailure:failure];
    } else {
        // Turn off
        if (instance->debug) {
            NSLog(@"Turning device off");
        }
        [self toggleDeviceWithID:name toggle:@"off" withSuccess:success andFailure:failure];
    }
}

- (void)toggleDevice:(NSString *)name toggle:(NSString *)t withSuccess:(requestSuccess)success andFailure:(requestFailure)failure
{
     [self toggleDeviceWithID:name toggle:t withSuccess:success andFailure:failure];
}

- (int)activatedDevices {
    int count = 0;
    for (NSString *s in [instance->devices valueForKeyPath:@"list.status"]) {
        if ([s isEqualToString:@"1"]) {
            count++;
        }
    }
    return count;
}


@end
