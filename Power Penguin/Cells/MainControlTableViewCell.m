//
//  MainControlTableViewCell.m
//  Power Penguin
//
//  Created by Mathias Jensen on 1/23/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import "MainControlTableViewCell.h"

@implementation MainControlTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [_img_l1 setImage:[UIImage imageWithSVGNamed:@"wifi-high" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES]];
    
    [_img_l2 setImage:[UIImage imageWithSVGNamed:@"bulb" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES]];
    
    NSArray *imgvs = @[_img_g1,_img_g2,_img_g3,_img_g4,_img_g6,_img_g7,_img_g8,_img_g9];
    
    NSArray *imgnms = @[@"clock", @"reminders", @"battery", @"plug", @"thermometer", @"support", @"refresh", @"settings"];
    
    for (NSInteger i = 0; i < [imgvs count]; i++) {
        UIImageView *view = [imgvs objectAtIndex:i];
        
        [view setImage:[UIImage imageWithSVGNamed:[imgnms objectAtIndex:i] targetSize:CGSizeMake(50, 50) fillColor:[UIColor whiteColor] cache:YES]];
    }
    
    _img_l1.animationImages = @[
                                [UIImage imageWithSVGNamed:@"wifi-none" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES],
                                [UIImage imageWithSVGNamed:@"wifi-low" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES],
                                [UIImage imageWithSVGNamed:@"wifi-mid" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES],
                                [UIImage imageWithSVGNamed:@"wifi-high" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES],
                                [UIImage imageWithSVGNamed:@"wifi-mid" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES],
                                [UIImage imageWithSVGNamed:@"wifi-low" targetSize:CGSizeMake(80, 80) fillColor:[UIColor whiteColor] cache:YES]
                                ];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
