//
//  ToggleTableViewCell.h
//  Power Penguin
//
//  Created by Mathias Jensen on 1/24/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBSwitch.h>

@interface ToggleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet MBSwitch *switchToggle;
@property (weak, nonatomic) IBOutlet UIImageView *imgToggleType;
@property (weak, nonatomic) IBOutlet UIView *viewIndicator;

@property (readwrite) bool prevState;

@property (weak, nonatomic) IBOutlet UILabel *labelDeviceName;
@property (weak, nonatomic) IBOutlet UILabel *labelDeviceType;

@end