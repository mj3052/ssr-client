//
//  ToggleTableViewCell.m
//  Power Penguin
//
//  Created by Mathias Jensen on 1/24/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import "ToggleTableViewCell.h"

@implementation ToggleTableViewCell

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // Switch
    [self.switchToggle setThumbTintColor:[UIColor colorWithWhite:0.783 alpha:1.000]];
    [self.switchToggle setOnTintColor:[UIColor colorWithRed:0.190 green:0.587 blue:0.695 alpha:1.000]];
    [self.switchToggle setOffTintColor:[UIColor colorWithWhite:0.271 alpha:1.000]];
    [self.switchToggle setTintColor:[UIColor colorWithWhite:0.271 alpha:1.000]];
    
    // Indicator
    [self.viewIndicator.layer setCornerRadius:2];
}


@end
