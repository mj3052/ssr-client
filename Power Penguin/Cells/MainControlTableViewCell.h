//
//  MainControlTableViewCell.h
//  Power Penguin
//
//  Created by Mathias Jensen on 1/23/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImage+SVG/UIImage+SVG.h>

@interface MainControlTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_l1;
@property (weak, nonatomic) IBOutlet UIImageView *img_l2;

@property (weak, nonatomic) IBOutlet UIImageView *img_g1;
@property (weak, nonatomic) IBOutlet UIImageView *img_g2;
@property (weak, nonatomic) IBOutlet UIImageView *img_g3;
@property (weak, nonatomic) IBOutlet UIImageView *img_g4;
@property (weak, nonatomic) IBOutlet UIImageView *img_g5;
@property (weak, nonatomic) IBOutlet UIImageView *img_g6;
@property(weak, nonatomic) IBOutlet UIImageView *img_g7;
@property (weak, nonatomic) IBOutlet UIImageView *img_g8;
@property (weak, nonatomic) IBOutlet UIImageView *img_g9;



@property (weak, nonatomic) IBOutlet UILabel *label_l1;
@property (weak, nonatomic) IBOutlet UILabel *label_l2;

@property (weak, nonatomic) IBOutlet UILabel *label_g1;
@property (weak, nonatomic) IBOutlet UILabel *label_g2;
@property (weak, nonatomic) IBOutlet UILabel *label_g3;
@property (weak, nonatomic) IBOutlet UILabel *label_g4;
@property (weak, nonatomic) IBOutlet UILabel *label_g5;
@property (weak, nonatomic) IBOutlet UILabel *label_g6;
@property (weak, nonatomic) IBOutlet UILabel *label_g7;
@property (weak, nonatomic) IBOutlet UILabel *label_g8;
@property (weak, nonatomic) IBOutlet UILabel *label_g9;


@property (weak, nonatomic) IBOutlet UIView *view_l1;
@property (weak, nonatomic) IBOutlet UIView *view_l2;

@property (weak, nonatomic) IBOutlet UIView *view_g1;
@property (weak, nonatomic) IBOutlet UIView *view_g2;
@property (weak, nonatomic) IBOutlet UIView *view_g3;
@property (weak, nonatomic) IBOutlet UIView *view_g4;
@property (weak, nonatomic) IBOutlet UIView *view_g5;
@property (weak, nonatomic) IBOutlet UIView *view_g6;
@property (weak, nonatomic) IBOutlet UIView *view_g7;
@property (weak, nonatomic) IBOutlet UIView *view_g8;
@property (weak, nonatomic) IBOutlet UIView *view_g9;

@end
