//
//  ViewController.m
//  Power Penguin
//
//  Created by Mathias Jensen on 1/20/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showIntro
{
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:[self setupIntroPages]];
    [intro setDelegate:self];
    
    
    [intro showInView:self.view animateDuration:1];
}

- (NSArray *)setupIntroPages
{
    EAIntroPage *page1 = [EAIntroPage page];
    
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Penguin"]];
    [page1.titleIconView setFrame:CGRectMake(page1.titleIconView.frame.origin.x, page1.titleIconView.frame.origin.y, 150, 150)];
    
    [(UIImageView *)page1.titleIconView setContentMode:UIViewContentModeScaleAspectFit];
    page1.titleIconPositionY = 100;
    
    page1.bgImage = [UIImage imageNamed:@"bg1"];
    
    page1.title = @"Welcome to Power Penguin!";
    
    page1.desc = @"This is a guide to get you started controlling your electronic devices wirelessly. If you already know how, feel free to skip it.";
    
    return @[page1];
}

@end
