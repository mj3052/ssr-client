//
//  MainTableViewController.h
//  Power Penguin
//
//  Created by Mathias Jensen on 1/22/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PowerPenguin.h"

#import "PPModalViewController.h"

#import "MainControlTableViewCell.h"
#import "ToggleTableViewCell.h"

@interface MainTableViewController : UITableViewController

- (void)gridActionFromSender:(id)sender;

@end
