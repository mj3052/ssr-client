//
//  MainTableViewController.m
//  Power Penguin
//
//  Created by Mathias Jensen on 1/22/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import "MainTableViewController.h"

@interface MainTableViewController ()

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Power Penguin";
    
    // Setup views
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.106 green:0.109 blue:0.120 alpha:1.000]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.tableView setPagingEnabled:YES];
    
    [self connectAndLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    else if(section == 1) {
        if ([[PowerPenguin instance] isConnected]) {
            return [[[PowerPenguin instance] deviceList] count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        MainControlTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Control" forIndexPath:indexPath];
        
        // Tile view references in an array. We'll loop through these to prevent duplicate code
        NSArray *views = @[cell.view_g1, cell.view_g2, cell.view_g3, cell.view_g4, cell.view_g5, cell.view_g6, cell.view_g7, cell.view_g8, cell.view_g9];
        
        // For every tile an invisible button should be created as overlay to detect actions
        for (NSInteger i = 0; i < [views count]; i++) {
            
            // The button should be positioned above the tile view
            UIView *view = [views objectAtIndex:i];
            CGRect frame = view.frame;
            
            UIButton *button = [[UIButton alloc] initWithFrame:frame];
            
            // Each button is assigned a tag. That way we know which one is pressed.
            button.tag = i;
            
            [button addTarget:self action:@selector(gridActionFromSender:) forControlEvents:UIControlEventTouchUpInside];
            
            // Using a clear background, alpha would cause the button to not work.
            [button setBackgroundColor:[UIColor clearColor]];
            
            [self.view addSubview:button];
        }
        
        int devicesOn = [[PowerPenguin instance] activatedDevices];
        
        if(devicesOn > 1) {
            [cell.label_l2 setText:[NSString stringWithFormat:@"%d devices are on", devicesOn]];
        } else if(devicesOn == 1) {
            [cell.label_l2 setText:@"1 device is on"];
        } else {
            [cell.label_l2 setText:@"No devices are on"];
        }
        
        return cell;
    }
    else {
        ToggleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Toggle" forIndexPath:indexPath];
        [cell setBackgroundColor:[UIColor colorWithRed:0.106 green:0.109 blue:0.120 alpha:1.000]];
        
        
        // Get a dictionary containing device info for current row. Should be a __weak reference, let's not leak
        __weak NSDictionary *device = [[[PowerPenguin instance] deviceList] objectAtIndex:indexPath.row];
        
        // Set name in label
        [cell.labelDeviceName setText:[device valueForKey:@"name"]];
        
        // We're going to tag each toggle. That way we can reuse code when handling a value change.
        [cell.switchToggle setTag:indexPath.row];
        
        // Check the status of the device and set the toggle
        if ([[device valueForKey:@"status"] isEqualToString:@"1"]) {
            [cell.switchToggle setOn:YES];
            [cell.viewIndicator.layer setBackgroundColor:[UIColor colorWithRed:0.190 green:0.587 blue:0.695 alpha:1.000].CGColor];
            
        } else {
            [cell.switchToggle setOn:NO];
            [cell.viewIndicator.layer setBackgroundColor:[UIColor colorWithWhite:0.271 alpha:1.000].CGColor];
        }
        
        // Adds a target selector for when the switch is toggled
        [cell.switchToggle addTarget:self action:@selector(switchChangedFromSender:) forControlEvents:UIControlEventValueChanged];

        // Image
        [cell.imgToggleType setImage:[UIImage imageWithSVGNamed:@"bulb" targetSize:CGSizeMake(50, 50) fillColor:[UIColor whiteColor] cache:YES]];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 603.0;
    }
    else if([[PowerPenguin instance] isConnected]){
        if (indexPath.section == 1 && indexPath.row == [[[PowerPenguin instance] deviceList] count]-1) {
            NSLog(@"Last cell");
            return 667.0 - (80 * [[[PowerPenguin instance] deviceList] count]);
        }
    }
    NSLog(@"80");
    return 80.0;
}

#pragma mark - Data loading
- (void)connectAndLoad
{
    [self connectShouldAnimate:YES];
    
    [[PowerPenguin instance] updateDevicesWithSuccess:^(NSDictionary *response) {
        [self connectShouldAnimate:NO];
        
        NSLog(@"Reload data");
        [self.tableView reloadData];
    } andFailure:nil];
}

#pragma mark - Button actions

- (void)gridActionFromSender:(id)sender
{
    NSInteger tag = [(UIButton *)sender tag];
    
    // Debug actions
    if ([[PowerPenguin instance] debug]) {
        if (tag == 0) {
            UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:[[PPModalViewController alloc] init]];
            
            [controller setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            [self.navigationController presentViewController:controller animated:YES completion:nil];
        }
        else if(tag == 1) {
            [self connectShouldAnimate:YES];
        }
        else if(tag == 2) {
            [self connectShouldAnimate:NO];
        }
        else if(tag == 3) {
            [self startPenguin];
        }
        else if(tag == 4) {
            [self stopPenguin];
        }
    }
    
}

- (void)switchChangedFromSender:(id)sender
{
    MBSwitch *s = (MBSwitch *)sender;
    NSInteger tag = [s tag];
    
    ToggleTableViewCell *cell = (ToggleTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:1]];
    
    NSString *state = @"on";
    
    if (s.isOn) {
        [UIView animateWithDuration:0.4 animations:^{
            [cell.viewIndicator.layer setBackgroundColor:[UIColor colorWithRed:0.190 green:0.587 blue:0.695 alpha:1.000].CGColor];
        }];
        
    } else {
        state = @"off";
        [UIView animateWithDuration:0.4 animations:^{
            [cell.viewIndicator.layer setBackgroundColor:[UIColor colorWithWhite:0.271 alpha:1.000].CGColor];
        }];
    }
    
    [[PowerPenguin instance] toggleDevice:[[[[PowerPenguin instance] deviceList] objectAtIndex:tag] valueForKey:@"id"] toggle:state withSuccess:nil andFailure:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - Animation

- (void)connectShouldAnimate:(BOOL)shouldAnimate
{
    MainControlTableViewCell *cell = [self control];
    
    [cell.img_l1 setAnimationDuration:3];
    
    if (shouldAnimate) {
        [cell.label_l1 setText:@"Connecting..."];
        [cell.img_l1 startAnimating];
    } else {
        [cell.label_l1 setText:@"Connected"];
        [cell.img_l1 stopAnimating];
    }
}


- (MainControlTableViewCell *)control
{
    return (MainControlTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

bool penguinIsAnimating;
- (void) spinWithOptions: (UIViewAnimationOptions) options {
    MainControlTableViewCell *cell = [self control];
    
    [UIView animateKeyframesWithDuration:1.0 delay:0.0
                                 options:UIViewKeyframeAnimationOptionCalculationModePaced | UIViewAnimationOptionCurveEaseInOut
                              animations:^{
                                  [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.0 animations:^{
                                      cell.img_g5.transform = CGAffineTransformMakeRotation(M_PI * 2.0f / 3.0f * -1);
                                  }];
                                  [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.0 animations:^{
                                      cell.img_g5.transform = CGAffineTransformMakeRotation(M_PI * 4.0f / 3.0f * -1);
                                  }];
                                  [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.0 animations:^{
                                      cell.img_g5.transform = CGAffineTransformIdentity;
                                  }];
                              }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (penguinIsAnimating) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}

- (void)startPenguin {
    if (!penguinIsAnimating) {
        penguinIsAnimating = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    }
}

- (void) stopPenguin {
    // set the flag to stop spinning after one last 90 degree increment
    penguinIsAnimating = NO;
}

@end
