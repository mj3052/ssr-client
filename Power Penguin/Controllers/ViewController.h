//
//  ViewController.h
//  Power Penguin
//
//  Created by Mathias Jensen on 1/20/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <EAIntroView.h>

@interface ViewController : UIViewController <EAIntroDelegate>

@property (weak, nonatomic) IBOutlet UIButton *button;

@end

