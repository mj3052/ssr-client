//
//  AppDelegate.h
//  Power Penguin
//
//  Created by Mathias Jensen on 1/20/15.
//  Copyright (c) 2015 Mathias Jensen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PowerPenguin.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

